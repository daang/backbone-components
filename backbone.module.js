'use strict';

// http://davidbcalhoun.com/2014/what-is-amd-commonjs-and-umd/
(function(root, factory) {
    if (typeof define === 'function' && define.amd) {
        // AMD
        define(['underscore', 'backbone'], factory);
    } else if (typeof exports === 'object') {
        // Node, CommonJS-like
        module.exports = factory(require('underscore'), require('backbone'));
    } else {
        // Browser globals (root is window)
        root.returnExports = factory(root._, root.Backbone);
    }
}(this, function(_, Backbone) {

    var Module = Backbone.Module = function( components, options ) {

        this.cid = _.uniqueId('mdl');
        // Has a list of components that will start when the module starts ( if
        // they have { autostart : true } - which will be default )

        if( components ) {
            this.components = components;
        }


        this.initialize.apply(this, arguments);
    };

    // Borrow extend method from Backbone.Model since it Backbone does not share the method publicly
    Module.extend = Backbone.Model.extend;

    _.extend( Backbone.Module.prototype, {
        start : function( appHub ) {

            // console.log('I am starting' + this.cid);

/*            this.appHub = appHub.appHub;

            this.appHub.on('destroy',  this.destroy, this);*/

            _.each( this.components, function(component) {
                new component();
            }, this);
        },

        stop : function() {
            // console.log('I am stopping' + this.cid);
        },

        destroy : function() {
            // Somehow destroy the module
            console.log(' ------------ Destroying module -----------------');
        },

        initialize : function() {
            // Add some initialize here
        }
    });
}));
