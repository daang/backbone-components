'use strict';
/******************************/
/* IMPORTANT, to whomever is reading */

/* This Plugin is still in development and for any questions on how it works,
   why it works the way it works, and any other crazy stuff that falls to mind
   please ask, @Mario */

// Components library should be able to do the following

// 1. Have a list of registered components

// 2. Add new components to the list

// 3. Remove components from the list

// 4. Fetch a component from the list

// 5. Prepare a component

// 6. Start a component (render view) ?

// 7. Should have events ?

// 8. Component local event system (can create EventHub as a Mixin) +  hub local name is a component name
//
// 9. Component type - a blog has articles, each article is a component, articles
// have different tags, so maybe different types for a component are necessary.
//
// 10. Give component an el and let it roll
//
// 11. Add a way to style a component

/*******************************/
/* Componet properties         */
/*******************************/

/*
    Componet as an idea, should be a place to prepare a view, a template and a model
    thus it should be able to acept a view, a template and the model as parameters,
    instantiate a view and prepare the component
*/


// http://davidbcalhoun.com/2014/what-is-amd-commonjs-and-umd/
(function(root, factory) {
    if (typeof define === 'function' && define.amd) {
        // AMD
        define(['underscore', 'backbone'], factory);
    } else if (typeof exports === 'object') {
        // Node, CommonJS-like
        module.exports = factory(require('underscore'), require('backbone'));
    } else {
        // Browser globals (root is window)
        root.returnExports = factory(root._, root.Backbone);
    }
}(this, function(_, Backbone) {


    //Backbone.Components = (function(Backbone, $, _) {

    // List of components
    var Components = {};

    // A single component. Component is just a placeholder for a
    // UI widget. It does not care about implementation details
    // only acts as an API between the component and the outside
    // world.
    Components.Component = function(options) {
        // Component will want a view, a template, a model / collection and data to pass into the component
        options || (options = {});

        // Add a unique identifier to the component
        this.cid = _.uniqueId('cmp');
        this.prepared = null; // Prepared component
        this.preparedViewOptions = {}; // Options to pass to the view
        //this.eventHubName = this.cid; // Maybe add something more robust here, but this should guarantee a unique identifier

        // _.extend(this, _.pick(options, compOptions));

        // Prepare the component if a Backbone View is being created
        this.prepare();
        

        this.initialize.apply(this, arguments);
        
        
        // Decided to create another no-op method alongside initialize. 
        // Reason to split this in two methods is because initialize is
        // already an established method in Backbone world and did not
        // want to mix the two. Besides, it's easier to create a mixin
        // this way if multiple components have the same create method.
        
        this.create.apply( this, arguments );
        
        
        // Start the component
        this.start();

    };

    // List of Component options to be merged as properties.
    // This is a borrowed list from a Backbone.View since component is basically rendering a view
    // and returning the completed thing back, it stands to reason all view options can be added to the component
    // + a name!
    // var compOptions = [ 'view', 'model', 'collection', 'template', 'modelData', 'collectionData' ];

    var viewOptions = ['el', 'id', 'attributes', 'className', 'tagName', 'events', 'template'];

    // Borrow extend method from Backbone.Model since it Backbone does not share the method publicly
    Components.Component.extend = Backbone.Model.extend;

    _.extend(Components.Component.prototype, Backbone.Events, {

        // Some component specific methods

        /**
         * [create description]
         *
         * @param  Object data Optional data passed to the create component method
         *                     If data exist they will be added to the model / collection
         *                     instantiation process.
         *
         * @return {[type]}      [description]
         */
        prepare: function() {

            var parsedViewOptions = _parseViewOptions(this);

            // this.validate();

            // Pass any View related options from the Component to the View
            _.extend(this.preparedViewOptions, _.pick(this, viewOptions), parsedViewOptions);
        },

        // validate: function() {
            // In stead of asking for a view, ensure there is a view in the component
            // Add basic backbone view
            //if (typeof this.view === 'undefined') {
            //    throw new Error('A valid View must be provided to the Component');
            //}

            // At least one of these must be set
            // NEW: Template will no longer be required
            //if (typeof this.template === 'undefined' && typeof this.view.template === 'undefined') {
            //    throw new Error('A view needs to have a template!');
            //}
        // },

        /**
         * Create method is the only method that a component is required
         * to implement. Implement the creation process of the method there
         * depending on what UI layer you'd like to use.
         * 
         */
        create: function() {
/*            if (!this.prepared) {
                this.prepared = new this.view(this.preparedViewOptions);

                // Add the template to the view if it's defined in options
                if (this.template) {
                    this.prepared.template = this.template;
                }

                return this.prepared;
            } else {
                throw new Error('Component has already been created, use parse to add new data and re-render.');
            }*/
        },
        
/*
        parse: function(options) {
            options || (options = {});

            // Close the previously created view
            if (this.prepared) {
                this.prepared.close();
            }

            this.prepared = null;

            // Use the new data and re-create t1he view
            _.extend(this, _.pick(options, compOptions));

            return this.create();
        },*/

        // Destroy the component, unbind events and remove from component list
        stop: function() {
            if ( this.beforeStop ) {
                this.beforeStop.call(this);
            }
            
            // Add stuff here


            if ( this.onStop ) {
                this.onStop.call(this);
            }

        },
        
        start: function() {
            if ( this.beforeStart ) {
                this.beforeStart.call(this);
            }
            
            // Add stuff here


            if ( this.onStart ) {
                this.onStart.call(this);
            }
            
        },

        mixin: function(from) {

            var to = this.prepared;

            // Add all methods from mixin to the component, but don't override the methods in the component
            _.defaults(to, from);

            // and we do the same for events
            _.defaults(to.events, from.events);
        },
        
        // Get the state of the component from the main state
        getState : function() {
            
            var state =  Backbone.serviceProvider( 'stateService' ).get();

            return state.get( this.cid );
        },
        
        // Set the state of the component to the main state
        setState : function( data ) {
            var state =  Backbone.serviceProvider( 'stateService' ).get();

            state.set( this.cid, data );
        },

        // Initialize is an empty function by default. Override it with your own
        // initialization logic.
        initialize: function() {}
    });

    // Based on what is available prepare view options
    var _parseViewOptions = function(component) {
        var viewOptions = {};
        var modelData = component.modelData || {};
        var collectionData = component.collectionData || {};

        if (component.model) {
            viewOptions.model = new component.model(modelData);
        }

        if (component.collection) {
            viewOptions.collection = new component.collection(collectionData);
        }

        return viewOptions;
    };

    Backbone.Components = Components;
}));
