Backbone Components is a slightly opinionated architectural approach to building Backbone Applications.
* It takes ideas introduced in Flux / React ( robust event system that connects key parts of the application)
* Services concept influenced by Angular
* Mix of component based approach by Nicholas Zakas.

Backbone components is built in a way that follows core Backbone principles, so anyone comfortable with using Backbone will not have much problem grasping the concepts introduced here.
The following syntax is familiar:

	Backbone.Application.extend({
		... awesome code here
	});

or something similar to the Backbone View event hash

	Backbone.Service.extend({
		name : 'awesomeService',

		services : {
			'doSomeAwesomeService' : 'callbackToAwesomeService',
			'doAnotherService'     : 'doAnotherService'
		},

		callbackToAwesomeService : function() {
			return 'Yup I am awesome';
		},

		doAnotherService : function() {
			...
		}
	});

In principle the goal is to give a more opinionated way of organizing a backbone app with a bit of boilerplate procedure removed.
This is achieved by using specialized segments of the library:

* Backbone Application
* Backbone Module
* Backbone Component
* Backbone Service
* Backbone Events Hub

This is done by following a few concepts:

* hierarchy :: route > application > modules > components
* scoped communication :: entities within a route share a special connection via prepared event callbacks ( such a state change ) + the ability to add ad-hoc events
* modularity :: provide base way to organize an application that if followed can help share components written in backbone among projects and developers

### Backbone Application

Backbone Application is a top level object that keeps track of the list of Modules assigned to it. It is **directly** bound to a route ( a hash fragment in the URL ) so only one application can be active at a time ( though same application object can be re-used on different routes ). When a route is changed, the current application is destroyed ( or made dormant ) with all its modules and components, and the new application is started.

The point of the application, other than being a 'callback' to the route is to keep track of the module life-cycle that it has control over.

Methods:

**start()** : start the list of modules that have been assigned to the application

**beforeStart()** : optional method that an application can implement to add arbitrary functionality before the application is started

**onStart()** : optional method that an application can implement to add arbitrary functionality after the start procedure is over

**initialize()** : same as any other Backbone Initialize

### Backbone Module

Similar to Backbone Application, the point of a Module is to keep track of component life-cycle that it has control over.
Also, the module gets bound by a set of events towards the application and the Components using Backbone Events Hub to enable easy communication across application.

Methods :

**start()** : start the list of components that have been assigned to the module

**stop()** : stop the module and all components

**destroy()** : destroy the module and all the components, remove bindings etc

**initialize()** : same as any other Backbone Initialize

### Backbone Component

Backbone Component is the agnostic part of the application. It it the 'View' Layer of the application and does not care on the actual UI implementation you choose. The only thing that is required that proper events are fired towards the Module responsible for the component using the Events Hub.

'But we already have a Backbone View ? ' you might ask .. and that is true .. but the point is that you can still use a Backbone View, but you can also use React, Polymer, Web Components or any other UI Layer you see fit within the component, as long as you follow the contract between a Module and a Component.

This is a very potent thing, because this enables an easy way to either test or completely change the UI layer of the application without affecting any other part of the application. Wake up one morning and decide that React is the thing you want to see in your app, just change the implementation within the component and you're set .. the business logic of the application and pretty much all other aspects will not be affected by it.

Backbone component is an isolated piece of UI functionality .. how ever you want to implement it.

The best thing .. as long as the contract between a Module and a Component is kept, there should not be any reason why the component can't be a plug-and-play resource for another application.

### Backbone Service

Backbone.Service is a means of extracting a piece of functionality to an isolated part of the application that can be accessed from anywhere. It's very similar to Angular services if you use it through a service provider. It's a singleton object that does some processing and returns a result .. It can have it's members but should not be stateful.

	var dateService = Backbone.Service.extend({
		// register a services hash
		services : {
			'getDate' : 'getDateCallback'
		},

		// Now define callbacks to the service
		getDateCallback : function() {
			return new Date();
		}
	});

	// Register this service with a name .. it can now be accessed from anywhere in the application
	Backbone.serviceFactory('myDateService', dateService);

	// ... ... ... somewhere in a galaxy far away
	// Get the service instance using the same name that is was registered with
	var dateService = Backbone.serviceProvider('myDateService');

	console.log( dateService.getDate()); // Will print the current date

### Backbone Events Hub

The part that makes all this possible is an extension to Backbone.Events that is heavily influenced by Backbone.Radio except that Command has been removed ( I see now that they removed it as well so we're on the right track :) ) .. and some minor processing has been added. Under the hood, hubs work with channels, but the difference is that a hub has to be explicitly created before a channel is opened. It comes down to responsibility and permissions. It does not make sense that a Module can create a channel on the application level, this should be done only by Application .. Also multiple bindings can be added during hub initialization time, as opposed to Radio.Channel where bindings are added one at a time.

	Backbone.EventsHub.registerHub('myHubName', {
		'myCallback1' : {
			type : 'respond',
			callback : this.someCallbackMethod
			context : [optional] .. context to pass to the callback
		},

		'mySecondCallback' : {
			type : 'listen',
			callback : this.anotherCallback,
			context : [optional]
		},
		'myThirdCallback' : {
			type : 'on',
			callback : this.thirdCallback,
			context : [optional]
		}

		// ... more callbacks for this hub
		// ... more callbacks for this hub

	});


Somewhere in your application you can do:

	var targetHub = Backbone.EventsHub.useHub('myHubName');

	targetHub.request('myCallback1');
	targetHub.trigger('mySecondCallback');
	targetHub.trigger('myThirdCallback');

Later on it will be possible to add callback methods only during the hub creation, so later on in the code you won't be able to add new listeners, etc .. reason is to reduce the potential of registering callbacks for a channel all over the place (and that will happen)

## How does it fit together

The goal of this approach is to simplify the decision making process when building and application and add semantically sound pieces for building a large scale single page application.

Also it should provide a way to bootstrap a project quickly with tools that will follow the suggestions provided ( e.g. yeoman )

Demo and a To-do mvc app coming soon.
