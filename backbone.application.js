'use strict';

// http://davidbcalhoun.com/2014/what-is-amd-commonjs-and-umd/
(function(root, factory) {
    if (typeof define === 'function' && define.amd) {
        // AMD
        define(['underscore', 'backbone'], factory);
    } else if (typeof exports === 'object') {
        // Node, CommonJS-like
        module.exports = factory(require('underscore'), require('backbone'));
    } else {
        // Browser globals (root is window)
        root.returnExports = factory(root._, root.Backbone);
    }
}(this, function(_, Backbone) {

    // Everything will have a state .. and everything will have a binding when
    // a state changes ..
    //
    // Everything will have onStart, onStop, start, stop methods
    //
    // Also need application level dispatcher .. somewhere an event will be triggered
    // and all registered applications will listen to it
    Backbone.EventsHub.registerHub('Kernel');

    var __applications = [];

    var __applicationRouter = new Backbone.Router();

    var Application = Backbone.Application = function( options ) {
        
        // Use the service provider to proxy the state setting and fetching
        this.stateService = Backbone.serviceProvider('stateService');

        var route = options.routeName;

        this.getRoute = function() {
            return route;
        };

        this.modules = options.modules;

        this.cid = _.uniqueId('app');

        // this.appHub = Backbone.EventsHub.registerHub(route + this.cid);

        Backbone.EventsHub.useHub('Kernel').on( 'swapRoute', this.bindRoute, this );

        // Add a list of modules that will start when the application starts

        // Trigger all sorts of action events

        // If no modules are added to the application add default Module ?

        this.initialize.apply(this, arguments);
    };

    // Borrow extend method from Backbone.Model since it Backbone does not share the method publicly
    Application.extend = Backbone.Model.extend;

    _.extend(Application.prototype, Backbone.Events, {
        // TODO - this should be a private method
        bindRoute : function( newActiveRoute, routeParameters ) {
            var route = this.getRoute();

            // If I am currently active application shut me down
            if( this.amActive ) {
                // console.log(' Shut me down ' + route );
                this.stop();
                // this.amActive = false;
            }

            // If am new active application turn me on
            if( route === newActiveRoute ) {
                // console.log(' Turn me on', route );
                this.start();
                // this.amActive = true;
            }
        },

        start : function( modules ) {
            if ( this.beforeStart ) {
                this.beforeStart.call(this);
            }

            _.each( this.modules, function(module) {
                new module().start({ appHub : this.appHub }); // Maybe pass application state here .. something else ?
            }, this);

            this.amActive = true;

            if ( this.onStart ) {
                this.onStart.call(this);
            }
        },

        stop : function() {
            if ( this.beforeStop ) {
                this.beforeStop.call(this);
            } 
            
            this.amActive = false;
            
            if ( this.afterStop ) {
                this.afterStop.call(this);
            }
        },

        initialize : function(){/* No-op*/}
    });

    var _applicationManager = function( routeName ) {
        
        var appEl = Backbone.Kernel.Config.getConfig().appEl;

        document.getElementById(appEl).innerHTML = '';
        
        // Swap between application contexts .. shut down the current application and
        // start the next application

        // Not sure whether to bind to a route here or somewhere else to swap the
        // applications

        var argsArray = [];
        var args = arguments;

        for( var i = 0; i < args.length; i++ ) {
            argsArray.push(args[i]);
        }

        // Remove the 'route' information from the parameters that will be passed
        // to the event .. that information is the route information in case route
        // is parametrized
        argsArray.shift();

        Backbone.EventsHub.useHub('Kernel').trigger('swapRoute', routeName, argsArray );
    };

    /**
     * [registerApps description]
     * @param  {[type]} applicationArray [description]
     * @return {[type]}                  [description]
     *
     * var applicationArray = [
     *     {
     *         route     : 'home',
     *         routeName : 'home'
     *         instance  : ApplicationContructor
     *     },
     *     {
     *         route     : 'users',
     *         routeName : 'users'
     *         instance  : UsersConstructor
     *     }
     * ]
     */
    Backbone.registerApps = function( applicationArray, options ) {
        
        Backbone.Kernel.Config.setConfig(options);

        _.each( applicationArray, function( applicationData ) {

            this.route( applicationData.route, applicationData.routeName, _.partial( _applicationManager, applicationData.routeName ));

            new applicationData.instance({
                routeName : applicationData.routeName,
                modules   : applicationData.modules
            });

        }, __applicationRouter);

        Backbone.history.start();
    };

    // var applicationArray = [
    //     {
    //         route     : 'home/:id',
    //         routeName : 'home',
    //         instance  : Backbone.Application
    //     },
    //     {
    //         route     : 'users',
    //         routeName : 'users',
    //         instance  : Backbone.Application
    //     },
    //     {
    //         route     : 'contact',
    //         routeName : 'contact',
    //         instance  : Backbone.Application
    //     }
    // ]

    // Backbone.registerApps(applicationArray);
}));
