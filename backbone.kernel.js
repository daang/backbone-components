// Backbone.Kernel.sandbox
// Backbone.Kernel.state
// Backbone.Kernel.config

Backbone.Kernel = (function( Kernel ) {
    
    // Config should not be exposed as a constructor
    
    var Config = function() {
        var config;
        
        var API = {
            setConfig : function( options ) {
                config = options;
            },
            
            getConfig : function() {
                return config;
            }
        }
        
        return API;
    }
    
    var Sandbox = function() {};
    
    // Add function to the sandbox
    Sandbox.prototype.add = function() {
        
    }
    
    // Provide an API to keep the state of the application
    var State = function() {
        this.state = {};
    };
    
    // Set the state
    State.prototype.set = function( key, data ) {
        this.state[key] = data;
    }
    
    // Set the state
    State.prototype.get = function( key ) {
        return this.state[key];
    }
    
    var SandboxService = Backbone.Service.extend({
        
        initialize : function() {
            this.sandbox = new Sandbox;
        },
        
       services : {
            getSandbox : 'getSandbox'
       },
       
       getSandbox : function() {
           return this.sandbox;
       }
    });
    
    var StateService = Backbone.Service.extend({
        
        initialize : function() {
            this.state = new State();
        },
        
        services : {
            get : 'getState'
        },
        
        getState : function() {
            return this.state;
        }
    });
    
    Kernel.Config = new Config();
    
    Backbone.serviceFactory( 'sandboxService', SandboxService );
    Backbone.serviceFactory( 'stateService', StateService );
    
    return Kernel;
    
/*    var setConfig = function( options ) {
        config = options;
    }
    
    var getConfig = function() {
        return config;
    }
    
    var getState = function( key ) {
        return state.key;
    }
    
    var setState = function( key, data ) {
        state.key = data;
    }
    
    API = {
        setConfig : setConfig,
        getConfig : getConfig,
        setState  : setState,
        getState  : getState
    }
    
    return API;*/
})( {} );