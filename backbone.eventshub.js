'use strict';

// http://davidbcalhoun.com/2014/what-is-amd-commonjs-and-umd/
(function(root, factory) {
    if (typeof define === 'function' && define.amd) {
        // AMD
        define(['underscore', 'backbone'], factory);
    } else if (typeof exports === 'object') {
        // Node, CommonJS-like
        module.exports = factory(require('underscore'), require('backbone'));
    } else {
        // Browser globals (root is window)
        root.returnExports = factory(root._, root.Backbone);
    }
}(this, function(_, Backbone) {

    function makeCallback(callback) {
        return _.isFunction(callback) ? callback : function() {
            return callback;
        };
    }

    // An optimized way to execute callbacks.
    function callHandler(callback, context, args) {
        var a1 = args[0],
            a2 = args[1],
            a3 = args[2];
        switch (args.length) {
            case 0:
                return callback.call(context);
            case 1:
                return callback.call(context, a1);
            case 2:
                return callback.call(context, a1, a2);
            case 3:
                return callback.call(context, a1, a2, a3);
            default:
                return callback.apply(context, args);
        }
    }

    var eventSplitter = /\s+/;

    // An internal method used to handle Radio's method overloading for Requests and
    // Commands. It's borrowed from Backbone.Events. It differs from Backbone's overload
    // API (which is used in Backbone.Events) in that it doesn't support space-separated
    // event names.
    function eventsApi(obj, action, name, rest) {
        if (!name) {
            return false;
        }

        var results = [];

        // Handle event maps.
        if (typeof name === 'object') {
            for (var key in name) {
                results.push(obj[action].apply(obj, [key, name[key]].concat(rest)));
            }
            return results;
        }

        // Handle space separated event names.
        if (eventSplitter.test(name)) {
            var names = name.split(eventSplitter);
            for (var i = 0, l = names.length; i < l; i++) {
                results.push(obj[action].apply(obj, [names[i]].concat(rest)));
            }
            return results;
        }

        return false;
    }

    function openChannel( channelName, options ) {
        if (!channelName) {
            channelName = 'global';
        }

        if ( _hubChannels[channelName] ) {
            return _hubChannels[channelName];
        } else {
            return ( _hubChannels[channelName] = new EventsHub.Channel( channelName, options ) );
        }
    }

    var _hubChannels = {};

    var EventsHub = {};

    /**
     * [register description]
     * @param  {[type]} hubName Unique event hub name
     * @param  {[type]} options A list of event callbacks declaratively added during the
     *                          register phase
     * Backbone.EventsHub.registerHub('myHubName', {
     *     'myCallback1' : {
     *         type : 'respond',
     *         callback : this.someCallbackMethod
     *         context : [optional] .. context to pass to the callback
     *     }
     * })
     * @return {[type]}
     */
    EventsHub.registerHub = function( hubName, options ) {

        if ( !hubName ) {
            throw new Error( 'Please provide a name for the hub.' );
        }

        if ( _hubChannels[hubName] ) {
            // Hub already exists

            throw new Error( 'Hub with this name:' + hubName + ' already exists, use another name to register hub.' );
        }

        return openChannel( hubName, options );
    };

    // Target a hub to use
    EventsHub.useHub = function( hubName ) {

        if ( !_hubChannels[hubName] ) {
            // Hub already exists
            throw new Error( 'Hub with this name:' + hubName + ' is not Registered. You can not use a hub that does not exist.' );
        }

        // Get the hub channel
        return openChannel(hubName);
    };

    // Remove hub from hub list
    EventsHub.destroyHub = function( hubName ) {
        if ( _hubChannels[hubName] ) {
            // Get the hub channel
            _hubChannels[hubName].reset();
            _hubChannels[hubName] = null;
        }
    };

    // Trigger an event but expect a return value
    EventsHub.request = function( name ) {
        // Which channel to trigger on (if no channel then global)
        // Which hub to trigger on
        var args = _.rest(arguments);
        var results = eventsApi(this, 'request', name, args);
        if (results) {
            return results;
        }

        var requests = this._requests;

        // If the request isn't handled, log it in DEBUG mode and exit
        if (requests && (requests[name] || requests['default'])) {
            var handler = requests[name] || requests['default'];
            args = requests[name] ? args : arguments;
            return callHandler(handler.callback, handler.context, args);
        }
    };

    // A callback handler to a particular request
    EventsHub.respond = function(name, callback, context) {
        if (eventsApi(this, 'respond', name, [callback, context])) {
            return this;
        }

        this._requests || (this._requests = {});

        this._requests[name] = {
            callback: makeCallback(callback),
            context: context || this
        };

        return this;
    };

    // Remove handler(s)
    EventsHub.stopResponding = function() {

       if (eventsApi(this, 'stopResponding', name)) {
           return this;
       }

       delete this._requests;

       return this;
   };

   // Consider not making a channel publicly available
    EventsHub.Channel = function( channelName, options ) {
        this.channelName = channelName;

        if( options ) {
            // Go through the options object and set up event listeners

            _.each( options, function( option, key ) {

                switch( option.type ) {
                    case 'respond' :

                        if( option.context) {
                            this.respond(key, option.callback, option.context);
                        } else {
                            this.respond(key, option.callback);
                        }

                        break;

                    case 'listen' :

                        if( option.context) {
                            this.listenTo (this, key, _.bind( option.callback, option.context));
                        } else {
                            this.listenTo (this, key, option.callback);
                        }

                        break;

                    case 'on' :

                        if( option.context) {
                            this.on(key, option.callback, option.context);
                        } else {
                            this.on(key, option.callback);
                        }

                        break;

                    default :
                        console.log( 'Type ' + option.type +' for the event ' + key + 'is not supported! Pleae use: respond, listen, on' );
                }
            });
        }
    };

    // Add Backbone events to channel and EventHub methods
    _.extend(EventsHub.Channel.prototype, Backbone.Events, EventsHub, {

        // Remove all handlers from the messaging systems of this channel
        reset: function() {
            this.off();
            this.stopListening();
            this.stopResponding();
            return this;
        }
    });

    Backbone.EventsHub = EventsHub;

    // Add all eventhub methods to Backbone.View prototype
    _.extend(Backbone.View.prototype, Backbone.EventsHub);

}));

