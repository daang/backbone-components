'use strict';

// http://davidbcalhoun.com/2014/what-is-amd-commonjs-and-umd/
(function(root, factory) {
    if (typeof define === 'function' && define.amd) {
        // AMD
        define(['underscore', 'backbone'], factory);
    } else if (typeof exports === 'object') {
        // Node, CommonJS-like
        module.exports = factory(require('underscore'), require('backbone'));
    } else {
        // Browser globals (root is window)
        root.returnExports = factory(root._, root.Backbone);
    }
}(this, function(_, Backbone) {

    /**
     * 1. Service is instantiated only once and the event bindings are set up for the service
     *
     * 2. Service is lazy loaded to the application only when it's needed
     *
     * 3. Services manager will be responsible for setting up / shutting down service bindings
     *
     * 4. Only events will be used to get the service to do something, using request - respond pattern
     *
     * 5. Complex business logic can be extracted to services but usually this will be
     *    only data manipulation
     *
     * 6. Services can use resources to fetch data / prepare data etc
     *
     * 7. Should services have access to config object ?
     *
     * 8. There can be only one! ( highlander style ) .. else the event bindings will be messed up
     *
     * 9. Service is an API to what the Application will allow the component to use .. thus only
     *    the Application can set a list of available services.
     */

     // List of registered services ( not started yet )
    var __services = {};

    // Instantiated services
    var __activeServices = {};

    Backbone.serviceFactory = function( serviceName, serviceConstructor, options ) {
        // Check if this service is already in 'services'
        options || (options = {});

        if ( _.isEmpty( serviceName ) ) {
            throw new Error('Service name is required.');
        }

        if( _.contains( _.keys( __services), serviceName)) {
            throw new Error('A service with this name is already created');
        } else {

            var service;
            // Just store the constructor for the service ... constructor will be
            // called at a later stage when the service is actually used by an
            // Application for the first time
            service = __services[ serviceName ] = serviceConstructor;

            // Optionally activate the service during creation time
            if ( options.activate === true ) {
                service = __activeServices[ serviceName ] = new serviceConstructor( options );
            }

            return service;
        }
    };

    // Create an instance of the service or return a service instance previously created
    Backbone.serviceProvider = function( serviceName, options ) {
        if ( _.contains( _.keys( __activeServices ), serviceName ) ) {
            return __activeServices[ serviceName ];
        } else if ( _.contains( _.keys( __services ), serviceName ) ) {
            var service = new __services[ serviceName ]( options );
            __activeServices[ serviceName ] = service;
            return service;
        } else {
            // Service that is being requested is not registered through the service provider
            throw new Error( 'Requested service ' + serviceName + ' is not registered through service factory.' );
        }
    };
    
    Backbone.destroyService = function( serviceName ) {

        if ( _.isEmpty( serviceName ) ) {
            throw new Error( 'Service name is required.' );
        }

        var service = __services[ serviceName ];
        var activeService = __activeServices[ serviceName ];

        if ( activeService ) {
            activeService.destroy();
        }

        if ( service ) {
            delete __services[ serviceName ];
        }
    };

    // A service constructor
    Backbone.Service = function( options ) {

        options || (options = {});

        _.extend( this, _.pick( options, [ 'services' ]) );

        this.cid = _.uniqueId( 's' );

        // Remember to remove a reference to the hub
        this.hub = Backbone.EventsHub.registerHub( 'service' + this.cid );

        // Do your own initalize stuff in the service
        this.initialize.apply( this, arguments );

        this.delegateServices();
    };

    // Borrow extend method from Backbone.Model since it Backbone does not share the method publicly
    Backbone.Service.extend = Backbone.Model.extend;

    _.extend( Backbone.Service.prototype, {

        delegateServices : function() {

            if (!this.services) return;

            // Bind all services to their associated callbacks
            // Re-call this method in case new service callbacks are added
            // programatically

            // Clear all current bindings from the service
            this.hub.stopResponding();

            // Re-bind the respond events
            _.each( this.services, function( serviceCallback, serviceRequestName ) {
                // Should we check if callback is available ?

                var callback;

                if( _.isFunction(serviceCallback)) {
                    callback = serviceCallback;
                } else {
                    callback = this[serviceCallback];
                }

                this.hub.respond(serviceRequestName, callback, this);

                // A proxy to call a callback on a channel directly from the service
                // so in stead of calling the service callbacks like this:
                //
                // Backbone.serviceProvider('selectBoxDataService').hub.request('getSelectBoxData', 'some', 'params')
                //
                // You call it like this:
                //
                // Backbone.serviceProvider('selectBoxDataService').getSelectBoxData('some', 'params', 'sent')
                //
                // This just looks a bit more convenient, easier to read and reason about.
                // Also it does not give any clue as to what is happening in the background
                // and that is using the event channels to get callbacks
                this[serviceRequestName] = function() {

                    // Save a reference to the name
                    var locName = serviceRequestName;
                    var argsArray = [];
                    var args = arguments;

                    for( var i = 0; i < args.length; i++ ) {
                        argsArray.push(args[i]);
                    }

                    argsArray.unshift(locName);

                    return this.hub.request.apply(this.hub, argsArray);
                };

            }, this );
        },
        
        destroy : function() {
            // Clear all current bindings from the service
            this.hub.destroyHub();
        },

        initialize : function(){}
    });

    // Backbone.Service.extend({
    //     name : 'awesomeService',

    //     services : {
    //         'doSomeAwesomeService' : 'callbackToAwesomeService'
    //     },

    //     callbackToAwesomeService : function() {
    //         return 'Yup I am awesome';
    //     }
    // });

}));
